const express = require('express');
const app = express();
const port = process.env.PORT || 3993;

app.use('/', (req, res) => res.send('ROOT PAGE /'));

app.listen(port, () => console.log(`Auto DevOps Test listening on port ${port}!`));